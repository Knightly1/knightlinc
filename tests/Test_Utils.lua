-- Source packages one level up
local scriptPath = (debug.getinfo(1, "S").source:sub(2)):match("(.*[\\|/]).*$")
local package_path_inc = scriptPath .. '..\\?.lua'

if not string.find(package.path, package_path_inc) then
    package.path = package_path_inc .. ';' .. package.path
end

local Utils = require('Utils')

local valid_url_test = 'https://www.google.com/'
local bad_url_test = 'https://www.google.com/\"'

local valid_file_test = 'Test_Utils.lua'
local bad_file_test = 'Test!|;"_Utils.lua'

print ("Starting Utils testing...")
-- File
if not Utils.File.Exists(thisScript) then
    print("Utils.File.Exists Test Failed - 1")
end

if not Utils.File.Sanitize(valid_file_test) == valid_file_test then
    print("Utils.File.Sanitize Test Failed - 1")
end

if not Utils.File.Sanitize(bad_file_test) == valid_file_test then
    print("Utils.File.Sanitize Test Failed - 2")
end

-- Library
if not Utils.Library.Include('write') then
    print("Utils.Library.Include Test Failed - 1")
end

if Utils.Library.Include('flibbertygibbet') then
    print("Utils.Library.Include Test Failed - 2")
end

-- URL
if not Utils.URL.Sanitize(valid_url_test) == valid_url_test then
    print("Utils.URL.Sanitize Test Failed - 1")
end

if not Utils.URL.Sanitize(bad_url_test) == valid_url_test then
    print("Utils.URL.Sanitize Test Failed - 2")
end
print("Utils testing complete.")